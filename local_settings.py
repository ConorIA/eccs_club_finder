'''
Local settings to define the dictionary for scraping Ulife
'''

# Set the local directory to work under
working_dir = ""

# Define the dictionary and scores for look-up
scores = {
    'sustain': 5,
    'enviro': 3,
    'green': 4,
    'rights': 3,
    'animal': 3,
    'health': 3
    }
