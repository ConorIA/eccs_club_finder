from bs4 import BeautifulSoup as soup
import urllib.request
import re
import os
import pandas as pd
from local_settings import working_dir, scores

if working_dir != "" and os.getcwd() != working_dir:
    os.chdir(working_dir)

row_list = []

# Download all the "groups"
print(">>> Downloading Group Pages")

# Get the number of pages of clubs
url = "https://ulife.utoronto.ca/organizations/list/page/1/"
source = urllib.request.urlopen(url)
page = soup(source, "html.parser")
pages = int(re.findall(r'[0-9]+', str(page.find_all("a", text="End")))[0])

interesting_clubs = 0
total_clubs = 0

for pg in range(1, pages + 1):
    print(">>> Scraping page {} of {}".format(str(pg), str(pages)))
    if pg > 1:
        url = "https://ulife.utoronto.ca/organizations/list/page/" + str(pg)

        source = urllib.request.urlopen(url)
        page = soup(source, "html.parser")

    listing = page.find_all("ul", attrs={"class": "listing innerListing"})

    if not listing:
        print("No listings on this page.")
        next

    clubs = listing[0].find_all('li')
    for club in clubs:
        total_clubs += 1
        clubname = club.find('a').contents[0]
        cluburl = "https://ulife.utoronto.ca" + club.find('a')['href']
        print("Scraping page for {}".format(clubname))
        try:
            clubsource = urllib.request.urlopen(cluburl)
        except Exception:
            last_url = "ERROR"
            print(">>> Error scraping {0}:".format(cluburl))
            with open("errors.txt", "a") as text_file:
                text_file.write(cluburl + "\n")
            continue

        clubpage = soup(clubsource, "html.parser")
        dts = clubpage.find_all('dt')
        dds = clubpage.find_all('dd')

        try:
            [i for i, s in enumerate(dds) if "Environment and Sustainability" in s.contents[0]][0]
        except IndexError:
            self_select = False
        else:
            print("This club has self-identified as being interested in sustainability.")
            self_select = True

        try:
            desc = clubpage.find_all('p')[1].contents[0]
        except IndexError:
            print("This club has no description.")

        occurrences = {}
        for key in scores:
            m = re.findall(key, desc, re.IGNORECASE)
            occurrences[key] = len(m)

        total_score = 0
        for word in occurrences:
            total_score += scores.get(word.lower(), 0) * occurrences[word]

        if total_score == 0 and not self_select:
            continue

        print("Total score was {}.".format(total_score))
        interesting_clubs += 1
        contact = [i for i, s in enumerate(dts) if "Primary Contact" in s.contents[0]][0]
        contact = dds[contact].contents[0]

        try:
            email = [i for i, s in enumerate(dts) if "Group Email" in s.contents[0]][0]
        except IndexError:
            email = ""
        else:
            email = re.sub("mailto:", "", dds[email].contents[0]['href'])

        clubtype = [i for i, s in enumerate(dts) if "Organization Type" in s.contents[0]][0]
        clubtype = dds[clubtype].contents[0]
        campus = [i for i, s in enumerate(dts) if "Campus Association" in s.contents[0]][0]
        campus = dds[campus].contents[0]

        pattern = r'\b20[0-9]{2}\b'
        try:
            renewal_no = [i for i, s in enumerate(dds) if re.search(pattern, str(s.contents[0]))][0]
        except IndexError:
            renewal_no = "skip"
            renewal = ""
            interest = "<error>"

        if renewal_no != "skip":
            renewal_no = [i for i, s in enumerate(dds) if re.search(pattern, str(s.contents[0]))][0]
            renewal = dds[renewal_no].contents[0]
            interest_no = [i for i, s in enumerate(dts) if "Areas of Interest" in s.contents[0]][0]
            interest = ''
            for int in range(interest_no, renewal_no):
                if interest == '':
                    interest = interest + dds[int].contents[0]
                else:
                    interest = interest + " / " + dds[int].contents[0]

        try:
            website = [i for i, s in enumerate(dts) if "Website" in s.contents[0]][0]
        except IndexError:
            website = ""
        else:
            website = dds[website].contents[0]['href']

        row = {'Name': clubname,
               'Score': total_score,
               'Contact': contact,
               'Email': email,
               'Type': clubtype,
               'Campus': campus,
               'Interests': interest,
               'Description': desc,
               'Website': website,
               'Renewal Date': renewal}

        row_list.append(row)

df = pd.DataFrame(row_list, columns=['Name',
                                     'Score',
                                     'Contact',
                                     'Email',
                                     'Type',
                                     'Campus',
                                     'Interests',
                                     'Description',
                                     'Website',
                                     'Renewal Date'])

df.sort_values(by="Score", ascending=False).to_csv("club_list.csv", index=False)

print(">>> We found {} interesting clubs from a total of {}".format(interesting_clubs, total_clubs))
