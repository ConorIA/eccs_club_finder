# U of T Sustainability Club Identifier

This script was developed as a contribution to the President's Advisory Committee on the Environment, Climate Change, and Sustainability. It scrapes the U of T Ulife website's [listing of recognized student clubs](https://ulife.utoronto.ca/organizations) and identifies those that:

1) Identify as a sustainability-related club
2) Include keywords in their description pulled form a configurable scored dictionary (see the `local_settings.py` file)

The results are output to a `.csv` file.
